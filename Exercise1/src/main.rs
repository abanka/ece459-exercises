// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut sum = 0;
    while number > 0 {
        number -= 1;
        if number % multiple1 == 0 || number % multiple2 == 0 {
            sum += number;
        }
    }
    sum
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_10_5_3() {
        assert_eq!(sum_of_multiples(10, 5, 3), 23);
    }

    #[test]
    fn test_1000_5_3() {
        assert_eq!(sum_of_multiples(1000, 5, 3), 233168);
    }
}

fn main() {}