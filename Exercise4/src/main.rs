use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: u32 = 10;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    for i in 0..N {
        let txi = mpsc::Sender::clone(&tx);
        thread::spawn(move || {
            txi.send(format!("thread {}", i)).expect("moss")
        });
    }

    for _ in 0.. N {
        let received = rx.recv().unwrap();
        println!("Got: {}", received);
        thread::sleep(Duration::from_millis(1000));
    }
}
