use std::thread;
use std::time::Duration;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children: Vec<thread::JoinHandle<()>> = vec![];
    for i in 1..=N {
        let handle = thread::spawn(move || {
            thread::sleep(Duration::from_millis(200));
            println!("output from thread {}", i)
        });
        children.push(handle);
    }
    children.into_iter().for_each(|handle| {
        handle.join().unwrap()
    })
}
