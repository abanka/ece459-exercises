
// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut n_2 = 0;
    let mut n_1 = 1;
    let mut current;
    if n == 0 {
        return n_2
    }
    for _ in 2..=n {
        current = n_1 + n_2;
        n_2 = n_1;
        n_1 = current;
    }
    n_1
}


fn main() {
    println!("{}", fibonacci_number(10));
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_0() {
        assert_eq!(fibonacci_number(0), 0);
    }

    #[test]
    fn test_1() {
        assert_eq!(fibonacci_number(1), 1);
    }

    #[test]
    fn test_2() {
        assert_eq!(fibonacci_number(2), 1);
    }

    #[test]
    fn test_10() {
        assert_eq!(fibonacci_number(10), 55);
    }
}